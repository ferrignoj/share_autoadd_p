#This is a quick and dirty readme.

I suck at writing Readme, this is a universal truth.

To have the google drive adapter working you need a client_secret.json associated
to an application using google Drive api (refer to google api quick start: https://developers.google.com/drive/v3/web/quickstart/python).
This file must be placed in the lib/ directory

The legacy dropbox driver uses selenium webdriver along with phantomjs. 
Only a valid Dropbox account is needed. Dropbox released the V2 version of
their API so it is no needed anymore but it there for "history".

The new dropbox driver adapter (DropboxShareAddedApiV2) uses the APIv2 from 
dropbox. As now has been converted to use fully the python api, some things
do not work, but it do not seems my fault. Be aware that dropbox api
documentation for the python version sucks.
To use them you need only a valid Auth token (how to get it : https://www.dropbox.com/developers)

##configuration
The file config_default.py must be populated copied/renamed in config.py
and populated with the settings for:

- the mail to parse, along with the login information and the folder to check
- Valid Dropbox credentials (no more needed, but useful to remember dropbox credentials)
- Valid google credentials

##Needing

In order to run successfully this program you will need:

- A google application using google Drive API (https://developers.google.com/drive/web/quickstart/python)
	- The user of the api must have the "modify" permissions on the folder
	
- A dropbox application using dropbox API (https://www.dropbox.com/developers)
	- The user of the api must have the "modify" permissions on the folder
	- For some endpoint the application has to have full access

- There is a requirements.txt for the one that want to use virtualenv 

No more needed (legacy version of dropbox)
- PhantomJS on the system (or you could edit DropboxShareAdder.py )
- An account for dropbox.

###To install the google api requisites
`pip install --upgrade google-api-python-client`


###To install the dropbox requisites 
`pip install --upgrade dropbox`

###To install the dropbox requisites (legacy)
`phantomjs`
`pip install --upgrade selenium`

###Modules overview

The Single modules for Dropbox and Google drive are self contained, this means
that you can run them as single python scripts, this is quite useful for testing.

This is also useful for the first start of the scripts, since you will have to grant 
permission to the folders (just for google drive ) to the user associated
with the different apps.

There is a ErrorManager, this was used initially to "save" users that had not been
possible to add due to timeout in the original implementation of the DropboxAdder.
It will give the possibility to save username(aka: mail) and folder of the user to be added,
if the user is added more than max_insertion times it will raise an exception;
this was done to check the timeout for the adder, but it is also useful to check
possible errors.

The Email parser is a specialized class made to deal with the registration mail.
It will parse a specific folder on the imap server and fetch the mail returning a
list of dict with mail and folder.
