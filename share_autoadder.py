import datetime

from lib import EmailParser
from lib import DriveShareAdder
from lib import ErrorManager
from lib import DropboxShareAddedApiV2

from config import email
from config import google_drive
from config import dropbox
from config import error_manager as em

import sys


def main(r_debug=False):

    # Only for remote debug
    if r_debug:
        import pydevd
        pydevd.settrace('192.168.1.2',
                        port=7331,
                        stdoutToServer=True,
                        stderrToServer=True)

    print "########################## START AT: " + \
          datetime.datetime.now().strftime("%Y-%m-%d %H:%M  ") + \
          "##########################"

    mail_parser = EmailParser.EmailParser(
        email['host'],
        email['account'],
        email['password'],
        email['folder'])

    error_manager = ErrorManager.ErrorManager(
        em['error_db_path'],
        em['max_insertions'])

    dropbox_adder = DropboxShareAddedApiV2.DropboxShareAddedApiV2(
        None,
        dropbox['access_token']
    )

    gdrive_adder = DriveShareAdder.DriveShareAdder(
        None,
        None)

    # get previously not added emails
    emails = error_manager.get_list()

    mail_parser.login()
    emails.extend(mail_parser.process_mailbox('UNSEEN'))

    if emails:

        dropbox_adder.login()
        gdrive_adder.login()

        dropbox_adder.set_share_folder_name(dropbox['folder'])
        gdrive_adder.set_share_folder_name(google_drive['folder'])

        for line in emails:
            if line['folder'] == 'drive':
                try:
                    gdrive_adder.add_to_share(line['email'], role="writer")
                    error_manager.remove(line)
                    print line['email'] + " added to Google Drive"

                except DriveShareAdder.ClientNotAddedError:
                    try:
                        error_manager.append(line)
                        print "%s not added to Drive, scheduled for next run" % line['email']

                    except ErrorManager.MaxInsertion:
                        error_manager.remove(line)
                        print "ERROR: %s not added to Drive" % line['email']
            else:
                try:
                    dropbox_adder.add_to_share(line['email'])
                    error_manager.remove(line)
                    print line['email'] + " added to Dropbox"

                except DropboxShareAddedApiV2.ClientNotAddedError:
                    try:
                        error_manager.append(line)
                        print "%s not added to Dropbox, scheduled for next run" % line['email']

                    except ErrorManager.MaxInsertion:
                        error_manager.remove(line)
                        print "ERROR: %s not added to Dropbox" % line['email']

    else:
        print "Nothing to process"

    mail_parser.clean()
    gdrive_adder.close()
    dropbox_adder.close()
    error_manager.close()

    print "########################## END AT: " + \
          datetime.datetime.now().strftime("%Y-%m-%d %H:%M  ") + \
          "##########################"

if __name__ == "__main__":

    r_debug = False
    if len(sys.argv) > 1 and '-d' in sys.argv:
        r_debug = True
    main(r_debug)

