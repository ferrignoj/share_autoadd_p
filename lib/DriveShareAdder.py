import httplib2
import os

import oauth2client

from ShareAdder import ShareAdder
from apiclient import discovery
from apiclient import errors

from oauth2client import client
from oauth2client import tools


class DriveShareAdder(ShareAdder):
    """
        This class encapsulate the method needed to add users to a shared folder.
        It has only some simple methods but it gets the work done.
        On the first run, if the credentials are not present it will
        generate them opening the browser.
        It is heavily on the google "getting started" example.

    """

    _SCOPES = 'https://www.googleapis.com/auth/drive'
    _CLIENT_SECRET_FILE = 'client_secret.json'
    _APPLICATION_NAME = 'Shared folder Autoadder'
    __CREDENTIALS_DIR = '.credentials'
    __CREDENDIAL_FILE = 'drive-python.json'

    try:
        import argparse
        flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    except ImportError:
        flags = None

    def get_credentials(self):
        """
        Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """

        working_dir = os.path.dirname(os.path.realpath(__file__))
        credential_dir = os.path.join(working_dir,
                                      '%s' % self.__CREDENTIALS_DIR)

        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)

        credential_path = os.path.join(credential_dir,
                                       self.__CREDENDIAL_FILE)

        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()

        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self._CLIENT_SECRET_FILE,
                                                  self._SCOPES)
            flow.user_agent = self._APPLICATION_NAME
            if self.flags:
                credentials = tools.run_flow(flow, store, self.flags)
            else:
                credentials = tools.run(flow, store)
        return credentials

    def _get_id(self):
        """
        Get the id for the selected folder

        :return: the id of the folder
        """
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v2', http=http)

        results = service.files().list(
            q="title contains '{}'".format(self._folder_name),
            fields='nextPageToken, items(id, title)').execute()

        items = results.get('items', [])
        if not items:
            print 'No files found.'

        else:
            for item in items:
                if self._folder_name in item['title']:
                    return item['id']

    def set_share_folder_name(self, name):
        """
        Set the name of the folder to share
        :param name: the name of the folder to share
        """
        self._folder_name = name

    def close(self):
        pass

    def login(self):
        self.get_credentials()

    def add_to_share(self, email, role='writer'):
        """
        Add the user identified by the mail to the share with a specific role.

        :param email: the mail of the user to ass
        :param role: the role of the user (see google api doc)
        """

        file_id = self._get_id()
        credentials = self.get_credentials()

        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v2', http=http)
        request = service.permissions().insert(fileId=file_id,
                                               sendNotificationEmails=True,
                                               body={
                                                     'value': email,
                                                     'type': 'user',
                                                     'role': role
                                                    })

        try:

            request.execute()

        except errors.HttpError, error:
            print 'An error occurred: %s' % error
        if email not in self.shared_with():
            raise ClientNotAddedError

    def shared_with(self):
        """
        Get the list of user (email) whit which the folder has been shared

        :return: a list of str containing email addresses
        """

        file_id = self._get_id()
        credentials = self.get_credentials()

        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v2', http=http)

        shared_with_list = []
        page_token = None
        while True:
            try:
                response = service.permissions().list(
                                                fileId=file_id,
                                                pageToken=page_token).execute()

                for user in response['items']:
                    try:
                        shared_with_list.append(user['emailAddress'].lower())
                    except KeyError:
                        pass

            except errors.HttpError:
                raise errors.HttpError

            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break
        return shared_with_list


class ClientNotAddedError(Exception):
    pass


if __name__ == '__main__':
    drive = DriveShareAdder(None, None)
    drive.login()
    drive.set_share_folder_name('test_folder')
    drive.add_to_share('test_mal@mail.com', role="writer")



