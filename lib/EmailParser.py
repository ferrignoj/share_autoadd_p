# Email parse module

import imaplib
import email


class EmailParser(object):

    def __init__(self, email_host, email_account, email_password, email_folder):
        """
        Standard builder for the class

        :param str email_host: the imap server contact
        :param str email_account: the email account to login
        :param str email_password: the password for the email account
        :param str email_folder: the folder from which get the messages
        :return:
        """
        self._EMAIL_HOST = email_host
        self._EMAIL_ACCOUNT = email_account
        self._EMAIL_PASSWORD = email_password
        self._EMAIL_FOLDER = email_folder
        self._M = imaplib.IMAP4_SSL(self._EMAIL_HOST)

    def login(self):
        """
        Login into the the mailbox

        """
        try:
            self._M.login(self._EMAIL_ACCOUNT, self._EMAIL_PASSWORD)
        except imaplib.IMAP4.error:
            print "e error"
            return None

    def process_mailbox(self, message_type='All'):
        """
        This method will process the mailbox and return
        a dict

        :param message_type: the message to filter
        :return: a dict containing email and sharetype
        """

        rv, data = self._M.select(self._EMAIL_FOLDER)
        if rv != "OK":
            return None
        # rv, data = M.search(None, 'UnSeen')
        rv, data = self._M.search(None, message_type)
        if rv != 'OK':
            print "No messages found!"
            return None
        information = []
        for num in data[0].split():
            rv, data = self._M.fetch(num, '(RFC822)')
            if rv != 'OK':
                return None
            msg = email.message_from_string(data[0][1])
            if msg.is_multipart():
                for payload in msg.get_payload():
                    pay = payload.get_payload(decode=True)
                    body = self._parse_mail_body(pay)
                    if body:
                        information.append(body)

            else:
                message = msg.get_payload()
                body = self._parse_mail_body(message)
                if body:
                    information.append(body)

        return information

    def list_mailboxes(self):
        """
        Print all the mailboxes for the account

        :return: Nothing
        """
        rv, mailboxes = self._M.list()

        if rv == 'OK':
            print "Mailboxes:"
            for line in mailboxes:
                assert isinstance(line, str)
                import re
                fields = re.search('.*"([^"]*)"', line)
                print fields.group(1)

    def clean(self):
        """
        Call this method at the end of the library use

        :return: nothing
        """
        self._M.close()
        self._M.logout()

    def _parse_mail_body(self, message, email_text="email", folder_text="cartella",email_k='email', folder_k='folder'):
        """
        This function will parse the string passed as argument.
        Will extract informations

        :param str message: the string to parse
        :param str email_text: the email field indicator
        :param str folder_text: the folder field indicator
        :return: a dict containing the informations
        """
        # lower the message, just in case
        message = message.lower()
        if email_text not in message:
            return None

        if folder_text not in message:
            return None
        # Skip html mail, they are a pain in the but to parse
        if "<html>" in message:
            return None

        data_dict = {}
        for line in message.splitlines():
            # avoid empty lines
            if not line.strip():
                continue
            # Skip all unwanted text
            if email_text not in line and folder_text not in line:
                continue
            # for security check if <mailto is present and remove it
            if '<mailto' in line:
                # Even if something strange happens rsplit()[0]
                # should return the original line
                line = line.rsplit('<', 1)[0]

            data = line.split(':')[1].strip()
            if email_text in line:
                #Check that the mail is correct
                import re
                correct = re.search(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', data)
                if not correct:
                    return None

                data_dict[email_k] = data
            else:
                data_dict[folder_k] = data
        return data_dict

if __name__ == "__main__":

    EMAIL_HOST = ''
    EMAIL_ACCOUNT = ""
    EMAIL_FOLDER = ""
    PASSWORD = ""

    mail_parser = EmailParser(EMAIL_HOST, EMAIL_ACCOUNT, PASSWORD, EMAIL_FOLDER)
    mail_parser.login()
    mail_parser.list_mailboxes()

    print "Processing mailbox...\n"
    ret = mail_parser.process_mailbox()
    if ret:
        for line in ret:
            print line["email"]
    else:
        print "error"
    mail_parser.clean()
