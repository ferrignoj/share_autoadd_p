from selenium import webdriver
from ShareAdder import ShareAdder
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from time import sleep


class DropboxShareAdder(ShareAdder):

    __PROFILE = "shareAdder"
    __LOGIN_URL = "https://www.dropbox.com/login"
    __BASE_FOLDER_URL = "https://www.dropbox.com/home/"
    # This keep rising and rising... To the star! (prev 240@16/10/15)
    __TIMEOUT = 300
    __CAPTCHA_TIMEOUT = 60

    def __init__(self, username, password):
        self.__LOGGED = False
        self._USERNAME = username
        self._PASSWORD = password
        self._driver = webdriver.PhantomJS('phantomjs')

    def login(self):

        self._driver.get(self.__LOGIN_URL)
        wait = WebDriverWait(self._driver, self.__TIMEOUT)
        # Fill email field
        wait.until(EC.presence_of_element_located((By.NAME, "login_email")))
        email_login_field = self._driver.find_elements_by_name("login_email")

        # Check for captcha
        try:
            wait_captcha = WebDriverWait(self._driver, self.__CAPTCHA_TIMEOUT)
            captcha = wait_captcha.until(
                EC.presence_of_element_located((By.XPATH,
                                                "//*[@id='recaptcha_image']")))
            if captcha:
                raise CaptchaException
        except TimeoutException:
            pass  # if we get there there "should" be not the captcha

        for field in email_login_field:
            if 'pyx' in field.get_attribute("id"):
                field.send_keys(self._USERNAME)
        # Fill password field
        wait.until(EC.presence_of_element_located((By.NAME, "login_password")))
        email_login_field = self._driver.find_elements_by_name("login_password")
        for field in email_login_field:
            if 'pyx' in field.get_attribute("id"):
                field.send_keys(self._PASSWORD)
                field.submit()

        # add to the methods a check if login
        self.__LOGGED = True
        sleep(20)

    def add_to_share(self, email):
        # Check if the login was successful
        if not self.__LOGGED:
            raise NotLoggedInError

        self._driver.get(self.__BASE_FOLDER_URL+self._folder_name)

        wait = WebDriverWait(self._driver, self.__TIMEOUT)

        wait.until(EC.presence_of_element_located((By.ID, 'global_share_button')))
        share_button = self._driver.find_element_by_id('global_share_button')
        share_button.click()

        try:
            wait.until(EC.presence_of_element_located((By.CLASS_NAME,
                                                       "new-collab-input")))
            email_add_field = self._driver.find_element_by_class_name(
                "new-collab-input")
            email_add_field.click()
            email_add_field.send_keys(email)

        except:
            wait.until(
                EC.presence_of_element_located((By.ID,
                                                "sharing-options-new-collab-input")))
            email_add_field = self._driver.find_element_by_id("sharing-options-new-collab-input")
            email_add_field.send_keys(email)

        wait.until(
            EC.presence_of_element_located((By.CLASS_NAME, 'confirm-button')))

        confirm_button = self._driver.find_element_by_class_name('confirm-button')
        confirm_button.click()

        wait = WebDriverWait(self._driver, (self.__TIMEOUT * 4))
        wait.until(EC.presence_of_element_located((By.XPATH,
                                                   "//input[@class='freshbutton-blue sf-action-done']")))

    def set_share_folder_name(self, name):
        self._folder_name = name

    def close(self):
        self._driver.close()
        self._driver.quit()


class CaptchaException(Exception):
    pass


class NotLoggedInError(Exception):
    pass


if __name__ == "__main__":
    print "Creating"
    adder = DropboxShareAdder("username", "password")
    print "calling add_to_share before login"
    try:
        adder.add_to_share("none@none.com")
    except NotLoggedInError:
        print "Not logged in. That is right"

    print "Logging in"
    try:
        adder.login()
    except CaptchaException:
        print "Damn Captcha!"
        adder.close()
        exit(-1)
    adder.set_share_folder_name("Test")
    print "Sharing"
    adder.add_to_share("test_mail@mail.com")
    adder.close()
