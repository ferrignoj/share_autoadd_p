from ShareAdder import ShareAdder
import dropbox
import dropbox.sharing
import dropbox.exceptions


# TODO Handle all the exceptions dropbox api throws
class DropboxShareAddedApiV2(ShareAdder):
    """
        This class encapsulate the method needed to add users to a shared folder.
        It has only some simple methods but it gets the work done.
        The access token can be hardcoded, can be passed as password
        for the constructor, or set with the set_access_token method.
        It uses the (terrible documented) dropbox python api.
    """

    """ This is the access token obtained once you create your
        dropbox application. It is essential to use dropbox api
    """
    __ACCESS_TOKEN = None

    def __init__(self, user, password):
        ShareAdder.__init__(self, user, password)
        if password:
            self.__ACCESS_TOKEN = password
        self.__dpx = None
        self._folder_name = None

    def close(self):
        pass

    def login(self):
        self.__dpx = dropbox.Dropbox(self.__ACCESS_TOKEN)

    def set_share_folder_name(self, name):
        self._folder_name = name

    def set_access_token(self, access_token):
        self.__ACCESS_TOKEN = access_token

    def add_to_share(self, email):
        """
        Add to share the user identified by mail
        :param email: the email of the user to add
        """
        folder_id = self._get_folder_id(self._folder_name)
        member = dropbox.sharing.AddMember(
            dropbox.sharing.MemberSelector.email(email),
            dropbox.sharing.AccessLevel.editor)
        try:
            self.__dpx.sharing_add_folder_member(
                folder_id, [member],
                custom_message="Sei stato aggiunto a " + self._folder_name)
        except Exception:  # TODO catch better exception
            raise ClientNotAddedError

        # if email not in self.invited_shared_with():
        #     raise ClientNotAddedError

    def _get_folder_id(self, name):
        """
        Return the unique id for the folder. It works only with already shared
        folder. I sincerely do not have the bravery to dig deeper in the python
        api documentation.

        :param name: the name of the folder for to which get the id
        :return:
        """
        shared_folders = self.__dpx.sharing_list_folders()
        for folder in shared_folders.entries:
            if name in folder.name:
                return folder.shared_folder_id

    def invited_shared_with(self):
        """
        This method should return a list of email for the user invited to the
        folder that didn't yep accepted the invitation. I say should because
        stopped working at some time.

        :return: a list of email addresses
        """
        # FIXME invited_shared_with return nothing...
        members = self.__dpx.sharing_list_folder_members(
            self._get_folder_id(self._folder_name))

        invitees = []

        for invitee in members.invitees:
            invitees.append(invitee.email)
        return invitees



class ClientNotAddedError(Exception):
    pass

if __name__ == "__main__":

    folder_to_test = 'test'
    drop = DropboxShareAddedApiV2(None, None)
    drop.login()
    drop.set_share_folder_name(folder_to_test)
    print "Id for {} is {}".format(folder_to_test,
                                   drop._get_folder_id(folder_to_test)
                                   )
    drop.add_to_share('email_to_invite@mail.com')

    for email in drop.invited_shared_with():
        print email
