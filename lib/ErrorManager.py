import json


class ErrorManager(object):
    """
        Use this class to save the various entry.
        On each insertion it will store the number of times a given entry
        has been added to the error list (all should be dictionary).
        If the given threshold is reached an
        @MaxInsertion is raised and the entry is removed from the list.

    """

    __ERROR_COUNT = 'error_count'

    def __init__(self, error_file_path='error_db', max_insertion=6):
        """
        Normal class initializer. See arguments for more details

        :param str error_file_path: the full path of the file containing the
                   information to parse. If it do not exists will be created.
                   The file should contain a valid json formatted text.
        :param int max_insertion: the maximum number of times that a
                   given entry can be inserted in the list.
        """

        self.__ERROR_DB_PATH = error_file_path
        self.__MAX_INSERTION = max_insertion

        try:
            with open(self.__ERROR_DB_PATH, 'r') as log:
                self.__list = json.load(log)
                log.close()
        except (IOError, ValueError):
            self.__list = []

        self.__ERROR_DB__ = open(self.__ERROR_DB_PATH, "w")

    def append(self, element):
        """
        Append the  argument to the internal list of save elements.
        This method will raise MaxInsertion if the insertion are more than the
        given threshold otherwise it will increase the internal counter.

        :param dict element: the element to insert in the internal list
        :raise exception MaxInsertion
        """
        assert isinstance(element, dict)
        # Check if @element is yet in the list
        idx = self._index_of(element)
        if idx is not None:
            el = self.__list[idx]
            el[self.__ERROR_COUNT] += 1
            if el[self.__ERROR_COUNT] > self.__MAX_INSERTION:
                self.__list.pop(idx)
                raise MaxInsertion()
        else:
            el_copy = element.copy()
            el_copy[self.__ERROR_COUNT] = 1
            self.__list.append(el_copy)

    def remove(self, element):
        """
        Remove the given element from the internal list

        :param dict element: the element to remove
        """
        idx = self._index_of(element)
        if idx is not None:
            self.__list.pop(idx)

    def close(self):
        """
        Save the internal list to file and close all.
        """
        json.dump(self.__list, self.__ERROR_DB__)
        self.__ERROR_DB__.close()

    def get_list(self):
        """
        Return the list of element in the internal list

        :return: list a list of element
        """

        clean_list = []
        for element in self.__list:
            dict_copy = dict(element)
            dict_copy.pop(self.__ERROR_COUNT, None)
            clean_list.append(dict_copy)
        return clean_list

    def clear_list(self):
        self.__list = []

    def _index_of(self, element):
        """
        Return the index of a given dict in the list.
        It is not possible to use dic1 == dict2 since the internal
        representation as a field more.
        There are more elegant way to check this, but I need an index!

        :param dict element: the element to search in the internal list
        :return: int the index of the element or None
        """

        for i, el in enumerate(self.__list):
            if all(key in el and element[key] == el[key]
                   for key in element):
                return i
        return None


class MaxInsertion(Exception):
    pass

if __name__ == "__main__":

    em = ErrorManager('/tmp/errordb')

    el1 = {'email': 'mail@gmail.com',
                    'cartella': 'dropbox'}
    el2 = {'email': 'mail2@gmail.com',
                    'cartella': 'drive',
                    'ba': 'ba'}
    try:
        em.append(el1)
    except MaxInsertion:
        print 'Max reached fpr %s' % el1['email']

    em.remove(el1)
    em.append(el1)

    try:
        em.append(el2)
    except MaxInsertion:
        print 'Max reached for %s' % el2['email']

    print '\nInternal list:'
    for line in em._ErrorManager__list:
        print line

    print '\nReturned list:'
    for line in em.get_list():
        print line
    em.close()
